package service;

import model.HistoryRequestStatus;
import model.Notification;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import repository.HistoryRequestStatusRepository;
import repository.NotificationRepository;

import java.util.List;

@Service
public class NotificationService {
    private final NotificationRepository notificationRepository;

    public NotificationService(NotificationRepository notificationRepository) {
        this.notificationRepository = notificationRepository;
    }

    public List<Notification> getAll(){
        return (List<Notification>) notificationRepository.findAll();
    }

    public ResponseEntity<?> findByID(long id){
        return ResponseEntity.ok(notificationRepository.findById(id));
    }

    public void deleteByID(long id){
        notificationRepository.deleteById(id);
    }

}

