package controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import service.CatalogService;
import service.CompanyService;

@RestController
public class CompanyController {
    private final CompanyService companyService;

    public CompanyController(CompanyService companyService) {
        this.companyService = companyService;
    }

    @GetMapping("/api/company")
    public ResponseEntity<?> getCompany() {
        return ResponseEntity.ok(companyService.getAll());
    }

    @GetMapping("/api/company/{id}")
    public ResponseEntity<?> findCompanyByID(@PathVariable long id){
        return ResponseEntity.ok(companyService.findByID(id));
    }

    @DeleteMapping("/api/deletecomp/{id}")
    public void deleteCompanyByID(@PathVariable long id){
        companyService.deleteByID(id);
    }
}
