package service;

import model.Catalog;
import model.Company;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import repository.CatalogRepository;
import repository.CompanyRepository;

import java.util.List;

@Service

public class CompanyService {
    private final CompanyRepository companyRepository;

    public CompanyService(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    public List<Company> getAll(){
        return (List<Company>) companyRepository.findAll();
    }

    public ResponseEntity<?> findByID(long id){
        return ResponseEntity.ok(companyRepository.findById(id));
    }

    public void deleteByID(long id){
        companyRepository.deleteById(id);
    }
}
