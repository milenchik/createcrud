package repository;

import model.Authorization;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository

public interface AuthRepository extends CrudRepository<Authorization, Long> {

    Authorization findById(long id);

    @Modifying
    @Transactional
    @Query("update Authorization a set a.username =:username where a.id =:ID")
    public void updateAuthorization(@Param("username") String username, @Param("ID") long id);
}



