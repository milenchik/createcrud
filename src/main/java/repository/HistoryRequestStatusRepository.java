package repository;

import model.HistoryRequestStatus;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface HistoryRequestStatusRepository extends CrudRepository<HistoryRequestStatus, Long> {
    @Modifying
    @Transactional
    @Query("update HistoryRequestStatus h set h.requestid =:requestid where h.id =:ID")
    public void updateByID(@Param("requestid") String requestid, @Param("ID") long id);
}
