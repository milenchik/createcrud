package service;

import model.Catalog;
import model.CatalogCase;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import repository.CatalogCaseRepository;
import repository.CatalogRepository;

import java.util.List;

@Service
public class CatalogService {
    private final CatalogRepository catalogRepository;

    public CatalogService(CatalogRepository catalogRepository) {
        this.catalogRepository = catalogRepository;
    }

    public List<Catalog> getAll(){
        return (List<Catalog>) catalogRepository.findAll();
    }

    public ResponseEntity<?> findByID(long id){
        return ResponseEntity.ok(catalogRepository.findById(id));
    }

    public void deleteByID(long id){
        catalogRepository.deleteById(id);
    }
}
