package controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import service.CatalogCaseService;

@RestController
public class CatalogCaseController {
    private final CatalogCaseService catalogCaseService;

    public CatalogCaseController(CatalogCaseService catalogCaseService) {
        this.catalogCaseService = catalogCaseService;
    }

    @GetMapping("/api/catalogCases")
    public ResponseEntity<?> getCatalogCases() {
        return ResponseEntity.ok(catalogCaseService.getAll());
    }

    @GetMapping("/api/catalogCase/{id}")
    public ResponseEntity<?> findCatalogCaseByID(@PathVariable long id){
        return ResponseEntity.ok(catalogCaseService.findByID(id));
    }

    @DeleteMapping("/api/deleteCatalogCase/{id}")
    public void deleteCatalogCaseByID(@PathVariable long id){
        catalogCaseService.deleteByID(id);
    }


}
