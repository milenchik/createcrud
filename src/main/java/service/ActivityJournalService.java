package service;

import model.ActivityJournal;
import model.Authorization;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import repository.ActivityJournalRepository;

import java.util.List;

@Service
public class ActivityJournalService {
    private final ActivityJournalRepository activityJournalRepository;

    public ActivityJournalService(ActivityJournalRepository activityJournalRepository) {
        this.activityJournalRepository = activityJournalRepository;
    }


    public List<ActivityJournal> getAll(){
        return (List<ActivityJournal>) activityJournalRepository.findAll();
    }
    public ResponseEntity<?> findActivityJournalByID(long id){
        return ResponseEntity.ok(activityJournalRepository.findById(id));
    }

    public void deleteActivityJournalByID(long id){
        activityJournalRepository.deleteById(id);
    }

    public void updateActivityJournalByID(long id, String eventType){
        activityJournalRepository.updateEventTypeByID(eventType, id);
    }

    public ActivityJournal create(ActivityJournal activityJournal){
        return activityJournalRepository.save(activityJournal);
    }

}
