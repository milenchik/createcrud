package service;

import model.HistoryRequestStatus;
import model.SearchKey;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import repository.HistoryRequestStatusRepository;
import repository.SearchKeyRepository;

import java.util.List;

@Service
public class SearchKeyService {
    private final SearchKeyRepository searchKeyRepository;

    public SearchKeyService(SearchKeyRepository searchKeyRepository) {
        this.searchKeyRepository = searchKeyRepository;
    }

    public List<SearchKey> getAll(){
        return (List<SearchKey>) searchKeyRepository.findAll();
    }

    public ResponseEntity<?> findByID(long id){
        return ResponseEntity.ok(searchKeyRepository.findById(id));
    }

    public void deleteByID(long id){
        searchKeyRepository.deleteById(id);
    }
}
