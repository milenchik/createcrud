package controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import service.DestructionActService;
import service.FondService;

@RestController
public class FondController {
    private final FondService fondService;

    public FondController(FondService fondService) {
        this.fondService = fondService;
    }

    @GetMapping("/api/fond")
    public ResponseEntity<?> getCompany() {
        return ResponseEntity.ok(fondService.getAll());
    }

    @GetMapping("/api/fond/{id}")
    public ResponseEntity<?> findCompanyByID(@PathVariable long id){
        return ResponseEntity.ok(fondService.findByID(id));
    }

    @DeleteMapping("/api/fond/{id}")
    public void deleteCompanyByID(@PathVariable long id){
        fondService.deleteByID(id);
    }

}
