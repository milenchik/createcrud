package service;

import model.FileRouting;
import model.HistoryRequestStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import repository.FileRoutingRepository;
import repository.HistoryRequestStatusRepository;

import java.util.List;

@Service
public class HistoryRequestStatusService {
    private final HistoryRequestStatusRepository historyRequestStatusRepository;

    public HistoryRequestStatusService(HistoryRequestStatusRepository historyRequestStatusRepository) {
        this.historyRequestStatusRepository = historyRequestStatusRepository;
    }

    public List<HistoryRequestStatus> getAll(){
        return (List<HistoryRequestStatus>) historyRequestStatusRepository.findAll();
    }

    public ResponseEntity<?> findByID(long id){
        return ResponseEntity.ok(historyRequestStatusRepository.findById(id));
    }

    public void deleteByID(long id){
        historyRequestStatusRepository.deleteById(id);
    }
}
