package controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import service.CatalogCaseService;
import service.CatalogService;

@RestController
public class CatalogController {
    private final CatalogService catalogService;

    public CatalogController(CatalogService catalogService) {
        this.catalogService = catalogService;
    }

    @GetMapping("/api/catalog")
    public ResponseEntity<?> getCatalogCases() {
        return ResponseEntity.ok(catalogService.getAll());
    }

    @GetMapping("/api/catalogCase/{id}")
    public ResponseEntity<?> findCatalogCaseByID(@PathVariable long id){
        return ResponseEntity.ok(catalogService.findByID(id));
    }

    @DeleteMapping("/api/deleteCatalog/{id}")
    public void deleteCatalogCaseByID(@PathVariable long id){
        catalogService.deleteByID(id);
    }
}
