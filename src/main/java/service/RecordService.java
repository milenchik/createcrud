package service;

import model.HistoryRequestStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import repository.HistoryRequestStatusRepository;
import repository.RecordRepository;

import java.util.List;

@Service
public class RecordService {
    private final RecordRepository recordRepository;

    public RecordService(RecordRepository recordRepository) {
        this.recordRepository = recordRepository;
    }

    public List<Record> getAll(){
        return (List<Record>) recordRepository.findAll();
    }

    public ResponseEntity<?> findByID(long id){
        return ResponseEntity.ok(recordRepository.findById(id));
    }

    public void deleteByID(long id){
        recordRepository.deleteById(id);
    }
}
