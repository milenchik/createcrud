package controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import service.DestructionActService;
import service.FileRoutingService;

@RestController
public class FileRoutingController {
    private final FileRoutingService fileRoutingService;

    public FileRoutingController(FileRoutingService fileRoutingService) {
        this.fileRoutingService = fileRoutingService;
    }

    @GetMapping("/api/filerouting")
    public ResponseEntity<?> getCompany() {
        return ResponseEntity.ok(fileRoutingService.getAll());
    }

    @GetMapping("/api/filerouting/{id}")
    public ResponseEntity<?> findCompanyByID(@PathVariable long id){
        return ResponseEntity.ok(fileRoutingService.findByID(id));
    }

    @DeleteMapping("/api/filerouting/{id}")
    public void deleteCompanyByID(@PathVariable long id){
        fileRoutingService.deleteByID(id);
    }
}
