package controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import service.CompanyUnitService;
import service.DestructionActService;

@RestController
public class DestructionActController {
    private final DestructionActService destructionActService;

    public DestructionActController(DestructionActService destructionActService) {
        this.destructionActService = destructionActService;
    }

    @GetMapping("/api/destr")
    public ResponseEntity<?> getCompany() {
        return ResponseEntity.ok(destructionActService.getAll());
    }

    @GetMapping("/api/destr/{id}")
    public ResponseEntity<?> findCompanyByID(@PathVariable long id){
        return ResponseEntity.ok(destructionActService.findByID(id));
    }

    @DeleteMapping("/api/destr/{id}")
    public void deleteCompanyByID(@PathVariable long id){
        destructionActService.deleteByID(id);
    }
}
