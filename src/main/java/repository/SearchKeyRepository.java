package repository;

import model.Nomenclature;
import model.SearchKey;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface SearchKeyRepository extends CrudRepository<SearchKey, Long> {
    @Modifying
    @Transactional
    @Query("update SearchKey c set c.name =:name where c.id =:ID")
    public void updateByID(@Param("name") String name, @Param("ID") long id);
}
