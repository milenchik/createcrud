package repository;

import model.Nomenclature;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface RecordRepository extends CrudRepository<Record, Long> {
    @Modifying
    @Transactional
    @Query("update Record r set r.createdby =:createdby where r.id =:ID")
    public void updateByID(@Param("createdby") String createdby, @Param("ID") long id);

}
