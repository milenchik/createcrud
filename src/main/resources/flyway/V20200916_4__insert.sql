ALTER TABLE case_table
    ADD CONSTRAINT fk1 FOREIGN KEY (locationid) REFERENCES location(id);

insert into ActivityJournal (eventType, objectType, objectId, createdTimestamp, createdBy, messageLevel, message)
            values ('type', 'objtype', 105, 1154548555, 27, '114', 'message');

insert into Location (row, line, columnN, box, companyUnitId, createdTimestamp, createdBy, updatedTimestamp, updatedBy)
            values ('104', '1255', '14', '4885', 124, 45445646464, 555, 1544488552, 1552);

insert into Company (nameRu, nameKz, nameEn, bin, parentId, fondId, createdTimestamp, createdBy, updatedTimestamp, updatedBy)
            values ('name', 'nameKz', 'nameengl', '454443544', 155115, 125, 151313131545, 48, 45445646488, 15);


insert into CompanyUnit (nameRu, nameKz, nameEn, parentId, year, companyId, codeIndex , createdTimestamp, createdBy, updatedTimestamp, updatedBy)
            values ('ru', 'kz', 'engl', 156332, 134646, 1, '154661516', 5445587841, 87, 078194850548785, 54548866);

insert into Notification (objectType, objectId, companyUnitId, userId, createdTimestamp, viewedTimestamp, isViewed, title, text, companyId)
            values ('TYPE', 2, 7, 9, 1216546131, 1542321534545, true, 'smcksmcklmc', 'dkdmkldm', 7);

insert into Case_table (caseNumber, caseTom, caseHeadingRu, caseHeadingKz, caseHeadingEn, startDate, finishDate, pageNumber, Eds, edsSignature, sendingNaf, deletionSign, limitedAccess, hash, version, idVersion, activeVersion , note, locationId, caseIndexId, inventoryId, destructionActId, structuralSubdivisionId, caseBlockchainAddress, addBlockchainDate, createdTimestamp, createdBy, updatedTimestamp, updatedBy)
            values ('1475', '155', 'vdmvlv', 'mkvvv', 'mdkvk', 18533255, 1318863, 125, true, 'vmdsvmds;v;', true, true, false, '154653', 1, 'mvdv;ldsv;', true, 'dvmldvm', 1, 2, 4, 3, 7, '15453431', 1578979832, 5646112, 541, 13684533, 15483);

insert into CaseIndex (caseIndex, titleRu, titleKz, titleEn, storageType, storageYear, note, companyUnitId, nomenclatureId, createdTimestamp, createdBy, updatedTimestamp, updatedBy)
            values ('954654654', 'd kldsvkldsvm', ';ld;ldv;dv', 'vmdklvdsm', 2, 2001, 'scsc', 4, 7, 9563165, 1454, 156538, 145);

insert into HistoryRequestStatus (requestId, status, createdTimestamp, createdBy, updatedTimestamp, updatedBy)
            values (15, 'jbnjnbjfbnf', 30522, 451, 22513555, 152);

insert into Request (requestUserId, responseUserId, caseId, caseIndexId, createdType, comment, status, timestamp, shareStart, shareFinish, favorite, updateTimestamp, updateBy, declineNote, companyUnitId, fromRequestId)
            values (1525, 1556, 2, 3, 'ndvdjviojv', 'ivdvoiodvi', 'mdklvmdlvdl', 365, 2145464, 29341545, false, 15646, 45631, 'dslsvm;lv', 7, 325);

insert into Record (number, type, companyUnitId, createdTimestamp, createdBy, updatedTimestamp, updatedBy)
            values ('21611', ';lvm;lvmdv', 7, 5343464, 145, 153454, 51);

insert into Fond (fondNumber, createdTimestamp, createdBy, updatedTimestamp, updatedBy)
            values ('2515', 15315415, 154848, 84533, 12);

insert into Users (authId, name, fullName, surname, secondName, status, companyUnitId, password, lastLoginTimestamp, iin, isActive, isActivated, createdTimestamp, createdBy, updatedTimestamp, updatedBy)
            values (2, 'klmklk', 'dvmd;lvmd;lvm', 'dvmdvml', ' dv;ldmv;lv', 'kbklfmbklfb', 5, 'mklmkmvmk', 125475, '1211215315', false, false, 453454513, 1233, 125452, 1);

insert into Share (requestId, note, senderId, receiverId, shareTimestamp)
            values (4, 'mvdvmd;vmds;vm', 1251, 13355, 11565464);

insert into CatalogCase (caseId, catalogId, companyUnitId, createdTimestamp, createdBy, updatedTimestamp, updatedBy)
            values (4, 5, 4, 654645, 22, 94636, 1);

insert into Catalog (nameRu, nameKz, nameEn, parentId, companyUnitId, createdTimestamp, createdBy, updatedTimestamp, updatedBy)
            values ('v,lv,v;l', 'v,vld,vldv,', 'vfvdsvds', 2222, 3, 662455, 066565, 266448, 256);


insert into Nomenclature ( nomenclatureNumber, year, nomenclatureSummaryId, companyUnitId, createdTimestamp, createdBy, updatedTimestamp, updatedBy)
            values ('61531531', 2000, 15, 4, 23165456, 1551, 215152, 1513);
insert into NomenclatureSummary (number, year, companyUnitId, createdTimestamp, createdBy, updatedTimestamp, updatedBy)
            values ('1541651', 2004, 6, 051516, 1515, 15151101, 1515);

insert into DestructionAct (actNumber, base, structuralSubdivisionId, createdTimestamp, createdBy, updatedTimestamp, updatedBy)
            values ('116120', ' cvdlvmdsv', 5, 02446613, 12, 15513, 133);

insert into File (name, type, size, pageCount, hash, isDeleted, fileBinaryId, createdTimestamp, createdBy, updatedTimestamp, updatedBy)
            values ('dvdsvsd', 'vdbdsb', 1456, 163, '131', true, 563, 14646, 112, 255632, 5214);

insert into FileRouting (fileId, tableName, tableId, type)
            values (15, 'zvdvdv', 2, 'nklnnn');

insert into SearchKey (name, companyUnitId, createdTimestamp, createdBy, updatedTimestamp, updatedBy)
            values ('jnnn', 4, 3153416, 54, 54654354, 5435);

insert into SearchKeyRouting (searchKeyId, tableName, tableId, type)
            values (4, 'kljojo', 14, ';m;mm');

insert into TempFiles (fileBinary, fileBinaryByte)
            values ('kjnkjn', 4);








