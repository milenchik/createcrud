package service;

import model.Catalog;
import model.FileRouting;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import repository.CatalogRepository;
import repository.FileRoutingRepository;

import java.util.List;

@Service

public class FileRoutingService {
    private final FileRoutingRepository fileRoutingRepository;

    public FileRoutingService(FileRoutingRepository fileRoutingRepository) {
        this.fileRoutingRepository = fileRoutingRepository;
    }

    public List<FileRouting> getAll(){
        return (List<FileRouting>) fileRoutingRepository.findAll();
    }

    public ResponseEntity<?> findByID(long id){
        return ResponseEntity.ok(fileRoutingRepository.findById(id));
    }

    public void deleteByID(long id){
        fileRoutingRepository.deleteById(id);
    }
}
