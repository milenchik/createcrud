package controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import service.LocationService;
import service.NotificationService;

@RestController
public class NotificationController {
    private final NotificationService notificationService;

    public NotificationController(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @GetMapping("/api/notif")
    public ResponseEntity<?> geNotif() {
        return ResponseEntity.ok(notificationService.getAll());
    }

    @GetMapping("/api/notif/{id}")
    public ResponseEntity<?> findNotifByID(@PathVariable long id){
        return ResponseEntity.ok(notificationService.findByID(id));
    }

    @DeleteMapping("/api/notif/{id}")
    public void deleteNotifByID(@PathVariable long id){
        notificationService.deleteByID(id);
    }

}

