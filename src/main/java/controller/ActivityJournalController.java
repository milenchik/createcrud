package controller;

import model.ActivityJournal;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import service.ActivityJournalService;

@RestController
public class ActivityJournalController {
    private final ActivityJournalService activityJournalService;

    public ActivityJournalController(ActivityJournalService activityJournalService) {
        this.activityJournalService = activityJournalService;
    }

    @GetMapping("/api/activityJournals")
    public ResponseEntity<?> findAll(){
        return  ResponseEntity.ok(activityJournalService.getAll());
    }



    @GetMapping("/api/activityJournal/{id}")
    public ResponseEntity<?> findActivityJournalByID(@PathVariable long id){
        return activityJournalService.findActivityJournalByID(id);
    }

    @DeleteMapping("/api/deleteActivityJournal/{id}")
    public void deleteActivityJournalByID(@PathVariable long id){
        activityJournalService.deleteActivityJournalByID(id);
    }

    @RequestMapping(value = "/lab2/updateActivityJournal/{id}/{eventType}", method = RequestMethod.GET)
    public void updateActivityJournalByID(@PathVariable("id") long id, @PathVariable("eventType") String eventType){
        activityJournalService.updateActivityJournalByID(id,eventType);
    }

    @PostMapping("/lab2/ActJour")
    public ResponseEntity<?> createActivityJournal(@RequestBody ActivityJournal activityJournal){
        return ResponseEntity.ok(activityJournalService.create(activityJournal));
    }

}
