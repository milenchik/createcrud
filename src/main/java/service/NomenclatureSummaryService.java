package service;

import model.HistoryRequestStatus;
import model.NomenclatureSummary;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import repository.HistoryRequestStatusRepository;
import repository.NomenclatureRepository;
import repository.NomenclatureSummaryRepository;

import java.util.List;

@Service
public class NomenclatureSummaryService {
    private final NomenclatureSummaryRepository nomenclatureSummaryRepository;

    public NomenclatureSummaryService(NomenclatureSummaryRepository nomenclatureSummaryRepository) {
        this.nomenclatureSummaryRepository = nomenclatureSummaryRepository;
    }

    public List<NomenclatureSummary> getAll(){
        return (List<NomenclatureSummary>) nomenclatureSummaryRepository.findAll();
    }

    public ResponseEntity<?> findByID(long id){
        return ResponseEntity.ok(nomenclatureSummaryRepository.findById(id));
    }

    public void deleteByID(long id){
        nomenclatureSummaryRepository.deleteById(id);
    }
}
