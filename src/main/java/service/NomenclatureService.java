package service;

import model.HistoryRequestStatus;
import model.Nomenclature;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import repository.HistoryRequestStatusRepository;
import repository.NomenclatureRepository;

import java.util.List;

@Service
public class NomenclatureService {
    private final NomenclatureRepository nomenclatureRepository;

    public NomenclatureService(NomenclatureRepository nomenclatureRepository) {
        this.nomenclatureRepository = nomenclatureRepository;
    }

    public List<Nomenclature> getAll(){
        return (List<Nomenclature>) nomenclatureRepository.findAll();
    }

    public ResponseEntity<?> findByID(long id){
        return ResponseEntity.ok(nomenclatureRepository.findById(id));
    }

    public void deleteByID(long id){
        nomenclatureRepository.deleteById(id);
    }
}
