package controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import service.CompanyService;
import service.CompanyUnitService;

@RestController
public class CompanyUnitController {
    private final CompanyUnitService companyUnitService;

    public CompanyUnitController(CompanyUnitService companyUnitService) {
        this.companyUnitService = companyUnitService;
    }

    @GetMapping("/api/companyUnit")
    public ResponseEntity<?> getCompany() {
        return ResponseEntity.ok(companyUnitService.getAll());
    }

    @GetMapping("/api/companyUnit/{id}")
    public ResponseEntity<?> findCompanyByID(@PathVariable long id){
        return ResponseEntity.ok(companyUnitService.findByID(id));
    }

    @DeleteMapping("/api/deletecompUnit/{id}")
    public void deleteCompanyByID(@PathVariable long id){
        companyUnitService.deleteByID(id);
    }
}
