package controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import service.LocationService;
import service.ShareService;

@RestController

public class ShareController {
    private final ShareService shareService;

    public ShareController(ShareService shareService) {
        this.shareService = shareService;
    }

    @GetMapping("/api/share")
    public ResponseEntity<?> geShare() {
        return ResponseEntity.ok(shareService.getAll());
    }

    @GetMapping("/api/share/{id}")
    public ResponseEntity<?> findShareByID(@PathVariable long id){
        return ResponseEntity.ok(shareService.findByID(id));
    }

    @DeleteMapping("/api/share/{id}")
    public void deleteShareByID(@PathVariable long id){
        shareService.deleteByID(id);
    }
}
