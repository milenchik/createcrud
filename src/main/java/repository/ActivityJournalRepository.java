package repository;

import model.ActivityJournal;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface ActivityJournalRepository extends CrudRepository<ActivityJournal, Long> {
    @Modifying
    @Transactional
    @Query("update ActivityJournal a set a.eventtype =:eventType where a.id =:ID")
    public void updateEventTypeByID(@Param("eventType") String EventType, @Param("ID") long id);

}
