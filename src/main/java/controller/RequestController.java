package controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import repository.RequestRepository;
import service.LocationService;
import service.RequestService;

@RestController

public class RequestController {
    private final RequestService requestService;

    public RequestController(RequestService requestService) {
        this.requestService = requestService;
    }

    @GetMapping("/api/request")
    public ResponseEntity<?> geRequest() {
        return ResponseEntity.ok(requestService.getAll());
    }

    @GetMapping("/api/request/{id}")
    public ResponseEntity<?> findRequestByID(@PathVariable long id){
        return ResponseEntity.ok(requestService.findByID(id));
    }

    @DeleteMapping("/api/request/{id}")
    public void deleteRequestByID(@PathVariable long id){
        requestService.deleteByID(id);
    }
}
