package repository;

import model.File;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface FileRepository  extends CrudRepository<File, Long> {
    @Modifying
    @Transactional
    @Query("update File f set f.fileid =:fileid where f.id =:ID")
    public void updateByID(@Param("fileid") String fileid, @Param("ID") long id);

}

