package controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import service.LocationService;
import service.RecordService;

@RestController
public class RecordController {
    private final RecordService recordService;

    public RecordController(RecordService recordService) {
        this.recordService = recordService;
    }

    @GetMapping("/api/record")
    public ResponseEntity<?> geRecord() {
        return ResponseEntity.ok(recordService.getAll());
    }

    @GetMapping("/api/record/{id}")
    public ResponseEntity<?> findRecordByID(@PathVariable long id){
        return ResponseEntity.ok(recordService.findByID(id));
    }

    @DeleteMapping("/api/record/{id}")
    public void deleteRecordByID(@PathVariable long id){
        recordService.deleteByID(id);
    }
}
