package controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import service.LocationService;
import service.UsersService;

@RestController

public class UsersController {
    private final UsersService usersService;

    public UsersController(UsersService usersService) {
        this.usersService = usersService;
    }

    @GetMapping("/api/users")
    public ResponseEntity<?> getUsers() {
        return ResponseEntity.ok(usersService.getAll());
    }

    @GetMapping("/api/users/{id}")
    public ResponseEntity<?> findUsersByID(@PathVariable long id){
        return ResponseEntity.ok(usersService.findByID(id));
    }

    @DeleteMapping("/api/users/{id}")
    public void deleteUsersByID(@PathVariable long id){
        usersService.deleteByID(id);
    }
}
