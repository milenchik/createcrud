package service;

import model.HistoryRequestStatus;
import model.Users;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import repository.HistoryRequestStatusRepository;
import repository.UsersRepository;

import java.util.List;

@Service
public class UsersService {
    private final UsersRepository usersRepository;

    public UsersService(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    public List<Users> getAll(){
        return (List<Users>) usersRepository.findAll();
    }

    public ResponseEntity<?> findByID(long id){
        return ResponseEntity.ok(usersRepository.findById(id));
    }

    public void deleteByID(long id){
        usersRepository.deleteById(id);
    }
}
