package service;

import model.Company;
import model.CompanyUnit;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import repository.CompanyRepository;
import repository.CompanyUnitRepository;

import java.util.List;

@Service
public class CompanyUnitService {
    private final CompanyUnitRepository companyUnitRepository;

    public CompanyUnitService(CompanyUnitRepository companyUnitRepository) {
        this.companyUnitRepository = companyUnitRepository;
    }

    public List<CompanyUnit> getAll(){
        return (List<CompanyUnit>) companyUnitRepository.findAll();
    }

    public ResponseEntity<?> findByID(long id){
        return ResponseEntity.ok(companyUnitRepository.findById(id));
    }

    public void deleteByID(long id){
        companyUnitRepository.deleteById(id);
    }
}
