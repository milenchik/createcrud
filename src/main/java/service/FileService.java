package service;

import model.CompanyUnit;
import model.File;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import repository.CompanyUnitRepository;
import repository.FileRepository;

import java.util.List;

@Service
public class FileService {
    private final FileRepository fileRepository;

    public FileService(FileRepository fileRepository) {
        this.fileRepository = fileRepository;
    }

    public List<File> getAll(){
        return (List<File>) fileRepository.findAll();
    }

    public ResponseEntity<?> findByID(long id){
        return ResponseEntity.ok(fileRepository.findById(id));
    }

    public void deleteByID(long id){
        fileRepository.deleteById(id);
    }
}

