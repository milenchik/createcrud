package service;

import model.HistoryRequestStatus;
import model.SearchKeyRouting;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import repository.HistoryRequestStatusRepository;
import repository.SearchKeyRepository;
import repository.SearchKeyRoutingRepository;

import java.util.List;

@Service
public class SearchKeyRoutingService {
    private final SearchKeyRoutingRepository searchKeyRoutingRepository;

    public SearchKeyRoutingService(SearchKeyRoutingRepository searchKeyRoutingRepository) {
        this.searchKeyRoutingRepository = searchKeyRoutingRepository;
    }

    public List<SearchKeyRouting> getAll(){
        return (List<SearchKeyRouting>) searchKeyRoutingRepository.findAll();
    }

    public ResponseEntity<?> findByID(long id){
        return ResponseEntity.ok(searchKeyRoutingRepository.findById(id));
    }

    public void deleteByID(long id){
        searchKeyRoutingRepository.deleteById(id);
    }
}
