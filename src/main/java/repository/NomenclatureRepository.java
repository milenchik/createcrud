package repository;

import model.Nomenclature;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface NomenclatureRepository extends CrudRepository<Nomenclature, Long> {
    @Modifying
    @Transactional
    @Query("update Nomenclature  n set n.nomenclaturenumber =:nomenclaturenumber where n.id =:ID")
    public void updateByID(@Param("nomenclaturenumber") String nomenclaturenumber, @Param("ID") long id);
}
