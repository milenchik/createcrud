package service;

import model.HistoryRequestStatus;
import model.TempFiles;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import repository.HistoryRequestStatusRepository;
import repository.TempFilesRepository;

import java.util.List;

@Service

public class TempFilesService {
    private final TempFilesRepository tempFilesRepository;

    public TempFilesService(TempFilesRepository tempFilesRepository) {
        this.tempFilesRepository = tempFilesRepository;
    }

    public List<TempFiles> getAll(){
        return (List<TempFiles>) tempFilesRepository.findAll();
    }

    public ResponseEntity<?> findByID(long id){
        return ResponseEntity.ok(tempFilesRepository.findById(id));
    }

    public void deleteByID(long id){
        tempFilesRepository.deleteById(id);
    }
}
