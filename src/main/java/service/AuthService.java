package service;

import model.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.AuthRepository;

import java.util.List;

@Service
public class AuthService {
    private final AuthRepository authRepository;

    public AuthService(AuthRepository authRepository) {
        this.authRepository = authRepository;
    }
public List<Authorization> getAll(){
        return (List<Authorization>) authRepository.findAll();
    }


    public Authorization getById(long id){
        return authRepository.findById(id);
    }
    public void deleteById(long id){
        authRepository.deleteById(id);
    }
    public Authorization createAuth(Authorization authorization){
        return authRepository.save(authorization);
    }
    public void updateAuthorization (String username,long id) {
        authRepository.updateAuthorization(username, id);
    }
}
