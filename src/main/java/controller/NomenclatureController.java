package controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import service.LocationService;
import service.NomenclatureService;

@RestController
public class NomenclatureController {
    private final NomenclatureService nomenclatureService;

    public NomenclatureController(NomenclatureService nomenclatureService) {
        this.nomenclatureService = nomenclatureService;
    }

    @GetMapping("/api/nomenclature")
    public ResponseEntity<?> geLocation() {
        return ResponseEntity.ok(nomenclatureService.getAll());
    }

    @GetMapping("/api/nomenclature/{id}")
    public ResponseEntity<?> findnomenclatureByID(@PathVariable long id){
        return ResponseEntity.ok(nomenclatureService.findByID(id));
    }

    @DeleteMapping("/api/nomenclature/{id}")
    public void deletenomenclatureByID(@PathVariable long id){
        nomenclatureService.deleteByID(id);
    }
}
