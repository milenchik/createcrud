package repository;

import model.Nomenclature;
import model.TempFiles;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface TempFilesRepository extends CrudRepository<TempFiles, Long> {
    @Modifying
    @Transactional
    @Query("update TempFiles t set t.filebinary =:filebinary where t.id =:ID")
    public void updateByID(@Param("filebinary") String filebinary, @Param("ID") long id);
}
