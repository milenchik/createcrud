package controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import service.CaseService;

@RestController
public class CaseController {
    private final CaseService caseService;

    public CaseController(CaseService caseEService) {
        this.caseService = caseEService;
    }

    @GetMapping("/api/cases")
    public ResponseEntity<?> getCases() {
        return ResponseEntity.ok(caseService.getAll());
    }

    @GetMapping("/api/case/{id}")
    public ResponseEntity<?> findCaseByID(@PathVariable long id){
        return ResponseEntity.ok(caseService.findById(id));
    }

    @DeleteMapping("/api/deleteCase/{id}")
    public void deleteCaseByID(@PathVariable long id){
        caseService.deleteById(id);
    }

    @RequestMapping(value = "/api/updateCase/{id}/{caseHeadingEN}", method = RequestMethod.GET)
    public void updateCaseByID(@PathVariable("id") long id, @PathVariable("caseHeadingEN") String caseHeadingEN){
        caseService.updateByID(id, caseHeadingEN);
    }


}
