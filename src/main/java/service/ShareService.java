package service;


import model.HistoryRequestStatus;
import model.Share;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import repository.HistoryRequestStatusRepository;
import repository.ShareRepository;

import java.util.List;

@Service
public class ShareService {
    private final ShareRepository shareRepository;

    public ShareService(ShareRepository shareRepository) {
        this.shareRepository = shareRepository;
    }

    public List<Share> getAll(){
        return (List<Share>) shareRepository.findAll();
    }

    public ResponseEntity<?> findByID(long id){
        return ResponseEntity.ok(shareRepository.findById(id));
    }

    public void deleteByID(long id){
        shareRepository.deleteById(id);
    }
}
