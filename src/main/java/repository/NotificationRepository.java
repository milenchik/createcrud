package repository;

import model.Nomenclature;
import model.Notification;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface NotificationRepository extends CrudRepository<Notification, Long> {
    @Modifying
    @Transactional
    @Query("update Notification n set n.objecttype =:objecttype where n.id =:ID")
    public void updateByID(@Param("objecttype") String objecttype, @Param("ID") long id);
}
