package repository;

import model.DestructionAct;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface DestructionActRepository extends CrudRepository<DestructionAct, Long> {
    @Modifying
    @Transactional
    @Query("update DestructionAct c set c.actnumber =:actnumber where c.id =:ID")
    public void updateByID(@Param("actnumber") String actnumber, @Param("ID") long id);
}
