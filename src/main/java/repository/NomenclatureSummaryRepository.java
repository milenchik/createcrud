package repository;

import model.Nomenclature;
import model.NomenclatureSummary;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface NomenclatureSummaryRepository extends CrudRepository<NomenclatureSummary, Long> {

}
