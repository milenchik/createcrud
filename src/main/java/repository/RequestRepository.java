package repository;

import model.Nomenclature;
import model.Request;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface RequestRepository extends CrudRepository<Request, Long> {
    @Modifying
    @Transactional
    @Query("update Request r set r.createdtype =:createdtype where r.id =:ID")
    public void updateByID(@Param("createdtype") String createdtype, @Param("ID") long id);
}
