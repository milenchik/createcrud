package controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import service.FileRoutingService;
import service.HistoryRequestStatusService;

@RestController
public class HistoryRequestStatusController {
    private final HistoryRequestStatusService historyRequestStatusService;

    public HistoryRequestStatusController(HistoryRequestStatusService historyRequestStatusService) {
        this.historyRequestStatusService = historyRequestStatusService;
    }

    @GetMapping("/api/history")
    public ResponseEntity<?> geHistory() {
        return ResponseEntity.ok(historyRequestStatusService.getAll());
    }

    @GetMapping("/api/history/{id}")
    public ResponseEntity<?> findHistoryByID(@PathVariable long id){
        return ResponseEntity.ok(historyRequestStatusService.findByID(id));
    }

    @DeleteMapping("/api/history/{id}")
    public void deleteHistoryByID(@PathVariable long id){
        historyRequestStatusService.deleteByID(id);
    }
}
