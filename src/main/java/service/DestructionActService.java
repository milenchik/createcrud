package service;

import model.CompanyUnit;
import model.DestructionAct;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import repository.CompanyUnitRepository;
import repository.DestructionActRepository;

import java.util.List;

@Service
public class DestructionActService {
    private final DestructionActRepository destructionActRepository;

    public DestructionActService(DestructionActRepository destructionActRepository) {
        this.destructionActRepository = destructionActRepository;
    }

    public List<DestructionAct> getAll(){
        return (List<DestructionAct>) destructionActRepository.findAll();
    }

    public ResponseEntity<?> findByID(long id){
        return ResponseEntity.ok(destructionActRepository.findById(id));
    }

    public void deleteByID(long id){
        destructionActRepository.deleteById(id);
    }
}
