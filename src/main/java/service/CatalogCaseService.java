package service;

import model.Authorization;
import model.Case;
import model.CatalogCase;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import repository.CatalogCaseRepository;

import java.util.List;

@Service
public class CatalogCaseService {
    private final CatalogCaseRepository catalogCaseRepository;

    public CatalogCaseService(CatalogCaseRepository catalogCaseRepository) {
        this.catalogCaseRepository = catalogCaseRepository;
    }

    public List<CatalogCase> getAll(){
        return (List<CatalogCase>) catalogCaseRepository.findAll();
    }

    public ResponseEntity<?> findByID(long id){
        return ResponseEntity.ok(catalogCaseRepository.findById(id));
    }

    public void deleteByID(long id){
        catalogCaseRepository.deleteById(id);
    }

}
