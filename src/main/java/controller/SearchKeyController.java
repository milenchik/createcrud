package controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import service.LocationService;
import service.SearchKeyService;

@RestController

public class SearchKeyController {
    private final SearchKeyService searchKeyService;

    public SearchKeyController(SearchKeyService searchKeyService) {
        this.searchKeyService = searchKeyService;
    }

    @GetMapping("/api/searchKey")
    public ResponseEntity<?> geLocation() {
        return ResponseEntity.ok(searchKeyService.getAll());
    }

    @GetMapping("/api/searchKey/{id}")
    public ResponseEntity<?> findLocationByID(@PathVariable long id){
        return ResponseEntity.ok(searchKeyService.findByID(id));
    }

    @DeleteMapping("/api/searchKey/{id}")
    public void deleteLocationByID(@PathVariable long id){
        searchKeyService.deleteByID(id);
    }
}
