package repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface CaseIndexRepository {
    @Modifying
    @Transactional
    @Query("update CaseIndex c set c.caseindex =:caseindex where c.id =:ID")
    public void updateByID(@Param("caseindex") String caseindex, @Param("ID") long id);
}
