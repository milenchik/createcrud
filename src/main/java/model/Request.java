package model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "request")

public class Request {
    @Id
    private long id;
    private long requestuserid;
    private long responseuserid;
    private long caseid;
    private long caseindexid;
    private String createdtype;
    private String comment;
    private String status;
    private long timestamp;
    private long sharestart;
    private long sharefinish;
    private boolean favorite;
    private long updatetimestamp;
    private long updateby;
    private String declinenote;
    private long companyunitid;
    private long fromrequestid;

}
