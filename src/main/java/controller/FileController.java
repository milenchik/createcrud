package controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import service.DestructionActService;
import service.FileService;

@RestController
public class FileController {
    private final FileService fileService;

    public FileController(FileService fileService) {
        this.fileService = fileService;
    }

    @GetMapping("/api/file")
    public ResponseEntity<?> getCompany() {
        return ResponseEntity.ok(fileService.getAll());
    }

    @GetMapping("/api/file/{id}")
    public ResponseEntity<?> findCompanyByID(@PathVariable long id){
        return ResponseEntity.ok(fileService.findByID(id));
    }

    @DeleteMapping("/api/file/{id}")
    public void deleteCompanyByID(@PathVariable long id){
        fileService.deleteByID(id);
    }
}
