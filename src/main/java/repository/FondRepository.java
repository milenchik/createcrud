package repository;

import model.Fond;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface FondRepository extends CrudRepository<Fond, Long> {
    @Modifying
    @Transactional
    @Query("update Fond f set f.fondnumber =:fondnumber where f.id =:ID")
    public void updateByID(@Param("fondnumber") String fondnumber, @Param("ID") long id);

}
