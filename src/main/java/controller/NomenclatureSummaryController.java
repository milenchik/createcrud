package controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import service.LocationService;
import service.NomenclatureSummaryService;

@RestController

public class NomenclatureSummaryController {
    private final NomenclatureSummaryService nomenclatureSummaryService;

    public NomenclatureSummaryController(NomenclatureSummaryService nomenclatureSummaryService) {
        this.nomenclatureSummaryService = nomenclatureSummaryService;
    }

    @GetMapping("/api/nomenclatureSummary")
    public ResponseEntity<?> geNomenclatureSummary() {
        return ResponseEntity.ok(nomenclatureSummaryService.getAll());
    }

    @GetMapping("/api/nomenclatureSummary/{id}")
    public ResponseEntity<?> findNomenclatureSummaryByID(@PathVariable long id){
        return ResponseEntity.ok(nomenclatureSummaryService.findByID(id));
    }

    @DeleteMapping("/api/nomenclatureSummary/{id}")
    public void deleteNomenclatureSummaryByID(@PathVariable long id){
        nomenclatureSummaryService.deleteByID(id);
    }
}
