package controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import service.LocationService;
import service.TempFilesService;

@RestController

public class TempFilesController {
    private final TempFilesService tempFilesService;

    public TempFilesController(TempFilesService tempFilesService) {
        this.tempFilesService = tempFilesService;
    }

    @GetMapping("/api/tempFiles")
    public ResponseEntity<?> getempFiles() {
        return ResponseEntity.ok(tempFilesService.getAll());
    }

    @GetMapping("/api/tempFiles/{id}")
    public ResponseEntity<?> findTempFilesByID(@PathVariable long id){
        return ResponseEntity.ok(tempFilesService.findByID(id));
    }

    @DeleteMapping("/api/tempFiles/{id}")
    public void deletetempFilesByID(@PathVariable long id){
        tempFilesService.deleteByID(id);
    }
}
