package service;

import model.Authorization;
import model.Case;
import org.springframework.stereotype.Service;
import repository.AuthRepository;
import repository.CaseRepository;

import java.util.List;

@Service
public class CaseService {
    private final CaseRepository caseRepository;

    public CaseService(CaseRepository caseRepository) {
        this.caseRepository = caseRepository;
    }

    public List<Case> getAll() {
        return (List<Case>) caseRepository.findAll();
    }


    public Case findById(long id) {
        return caseRepository.findById(id);
    }

    public void deleteById(long id) {
        caseRepository.deleteById(id);
    }

    public void updateByID(long id, String caseHeadingEN) {
        caseRepository.updateCaseHeadingEnByID(caseHeadingEN, id);
    }
}
