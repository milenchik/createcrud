package repository;

import model.Nomenclature;
import model.Users;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface UsersRepository extends CrudRepository<Users, Long> {
    @Modifying
    @Transactional
    @Query("update Users u set u.fullname =:fullname where u.id =:ID")
    public void updateByID(@Param("fullname") String fullname, @Param("ID") long id);

}
