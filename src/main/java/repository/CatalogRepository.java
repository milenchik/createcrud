package repository;

import model.Catalog;
import model.CatalogCase;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface CatalogRepository extends CrudRepository<Catalog, Long> {

    @Modifying
    @Transactional
    @Query("update Catalog c set c.createdby =:createdBy where c.id =:ID")
    public void updateCreatedByByID(@Param("createdBy") long createdBy, @Param("ID") long id);
}
