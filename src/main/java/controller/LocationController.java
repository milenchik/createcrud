package controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import service.HistoryRequestStatusService;
import service.LocationService;

@RestController
public class LocationController {
    private final LocationService locationService;

    public LocationController(LocationService locationService) {
        this.locationService = locationService;
    }

    @GetMapping("/api/location")
    public ResponseEntity<?> geLocation() {
        return ResponseEntity.ok(locationService.getAll());
    }

    @GetMapping("/api/location/{id}")
    public ResponseEntity<?> findLocationByID(@PathVariable long id){
        return ResponseEntity.ok(locationService.findByID(id));
    }

    @DeleteMapping("/api/location/{id}")
    public void deleteLocationByID(@PathVariable long id){
        locationService.deleteByID(id);
    }
}
