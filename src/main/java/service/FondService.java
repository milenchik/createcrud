package service;

import model.File;
import model.Fond;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import repository.FileRepository;
import repository.FondRepository;

import java.util.List;

@Service
public class FondService {
    private final FondRepository fondRepository;

    public FondService(FondRepository fondRepository) {
        this.fondRepository = fondRepository;
    }

    public List<Fond> getAll(){
        return (List<Fond>) fondRepository.findAll();
    }

    public ResponseEntity<?> findByID(long id){
        return ResponseEntity.ok(fondRepository.findById(id));
    }

    public void deleteByID(long id){
        fondRepository.deleteById(id);
    }
}
