package controller;

import model.Authorization;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import repository.AuthRepository;
import service.AuthService;

@RestController
public class AuthController {
private final AuthService authService;

    public AuthController(AuthService authService) {
        this.authService = authService;
    }
    @GetMapping("api/auth/{id}")
    public ResponseEntity<?> getById(@PathVariable long id){
       return ResponseEntity.ok(authService.getById(id));
    }
    @GetMapping("api/auth")
    public ResponseEntity<?> findAll(){
        return  ResponseEntity.ok(authService.getAll());
    }
    @DeleteMapping("api/auth/{id}")
    public void deleteById(@PathVariable long id){
        authService.deleteById(id);
    }

    @PostMapping("api/auth")
    public ResponseEntity<?> createAuth(@RequestBody Authorization authorization){
        return ResponseEntity.ok(authService.createAuth(authorization));
    }
    @RequestMapping(value="api/auth/update/{id}/{username}", method = RequestMethod.GET)
    public void updateAuthorization(@PathVariable("id") long id, @PathVariable("username") String username){
        authService.updateAuthorization (username, id);

    }

}
