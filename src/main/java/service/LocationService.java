package service;

import model.HistoryRequestStatus;
import model.Location;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import repository.HistoryRequestStatusRepository;
import repository.LocationRepository;

import java.util.List;

@Service
public class LocationService {
    private final LocationRepository locationRepository;

    public LocationService(LocationRepository locationRepository) {
        this.locationRepository = locationRepository;
    }

    public List<Location> getAll(){
        return (List<Location>) locationRepository.findAll();
    }

    public ResponseEntity<?> findByID(long id){
        return ResponseEntity.ok(locationRepository.findById(id));
    }

    public void deleteByID(long id){
        locationRepository.deleteById(id);
    }
}
