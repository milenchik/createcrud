package repository;

import model.Location;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Locale;

@Repository
public interface LocationRepository extends CrudRepository<Location, Long> {
    @Modifying
    @Transactional
    @Query("update Location l set l.row =:row where l.id =:ID")
    public void updateByID(@Param("row") String row, @Param("ID") long id);
}
